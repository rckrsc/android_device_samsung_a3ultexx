# Inherit some common LineageOS stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)
include vendor/cm/config/common.mk

$(call inherit-product, device/samsung/a3ultexx/full_a3ultexx.mk)

PRODUCT_BUILD_PROP_OVERRIDES += PRODUCT_NAME=a3ultexx TARGET_DEVICE=a3ulte

PRODUCT_NAME := lineage_a3ultexx
